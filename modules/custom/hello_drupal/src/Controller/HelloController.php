<?php
/**
 * @file
 * Contains \Drupal\hello_drupal\Controller\HelloController.
 */

namespace Drupal\hello_drupal\Controller;

use Drupal\Core\Controller\ControllerBase;

class HelloController extends ControllerBase {

	public function content($name) {
        return array(
            '#type' => 'markup',
            '#markup' => $this->t('Hello, @name!', array('@name' => $name)),
        );
  	}

	public function show_welcome() {
    
        //Aquí podemos acceder a la base de datos y controlar datos para enviarlos a nuestro bloque ;)
        return [
            '#theme' => 'show_hello_drupal',
            '#titulo' => 'Welcome page',
            '#descripcion' => 'Welcome from .routing.yml a twig in drupal'
        ];

	}
}