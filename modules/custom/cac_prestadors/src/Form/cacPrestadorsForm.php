<?php
//cac_prestadors/src/cacPrestadorsForm.php

namespace Drupal\cac_prestadors\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Desarrollo de un formulario en drupal 8
 * @author Ignacio Farré
 */
class cacPrestadorsForm extends FormBase
{

    /**
     * Counter keeping track of the sequence of method invocation.
     *
     * @var int
     */
    protected static $sequenceCounter = 0;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->displayMethodInvocation('__construct');
    }

    /**
     * Update form processing information.
     *
     * Display the method being called and it's sequence in the form
     * processing.
     *
     * @param string $method_name
     *   The method being invoked.
     */
    private function displayMethodInvocation($method_name)
    {
        self::$sequenceCounter++;
        //drupal_set_message(self::$sequenceCounter . ". $method_name");
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        $this->displayMethodInvocation('getFormId');
        return 'prestadors_llistat_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        \Drupal::logger('cac_prestadors')->notice('cacPrestadorsForm->buildForm() - ini -');
        $this->displayMethodInvocation('buildForm');

        $form['denominacioSocial'] = [
            '#type'     => 'textfield',
            '#title'    => $this->t('Nom / Denominació Social'),
            // '#description' => $this->t('Escriure text per cercar un prestador'),
            '#required' => false,
        ];
        $form['tipusPersonalitat'] = [
            '#type'     => 'select',
            '#title'    => $this->t('Tipus de prestador'),
            // '#description' => $this->t('Escollir tipus de prestador'),
            '#required' => false,
            '#options'  => array("-1" => t('Escollir tipus de prestador'), "1" => t('Públic'), "2" => t('Privat')),
        ];
        /*$form['nomServei'] = [
        '#type'     => 'textfield',
        '#title'    => $this->t('Nom del servei'),
        // '#default_value' => $this->t('Un gasto mas.'),
        '#required' => false,
        ];
        $form['accionista'] = [
        '#type'     => 'textfield',
        '#title'    => $this->t('Accionista'),
        // '#default_value' => $this->t('Un gasto mas.'),
        '#required' => false,
        ];*/

        $form['ajaxsubmit'] = [
            '#type'  => 'button',
            '#value' => 'Consulta',
            '#ajax'  => [
                'callback' => '::ajaxSubmit',
                'wrapper'  => 'table_wrapper',
                'progress' => array(
                    // Graphic shown to indicate ajax. Options: 'throbber' (default), 'bar'.
                    'type'    => 'throbber',
                    // Message to show along progress graphic. Default: 'Please wait...'.
                    'message' => $this->t('Please wait...'),

                )],
        ];
        //use-ajax-submit

        /*************/

        $uri      = "http://wsregpres.cac.cat/ws_regpres/api/llistaprestadors";
        $response = file_get_contents($uri);

        $dades = Json::decode($response);

        \Drupal::logger('cac_prestadors')->notice('Form get values');
        $tipusPersonalitat = $form_state->getValue('tipusPersonalitat');
        \Drupal::logger('cac_prestadors')->notice('*** TipusPersonalitat: ' . $tipusPersonalitat);
        $denominacioSocial = $form_state->getValue('denominacioSocial');
        \Drupal::logger('cac_prestadors')->notice('*** denominacioSocial: ' . $denominacioSocial);

        //validacion de datos del formulario
        if ($tipusPersonalitat >= 1) {
            $this->displayMethodInvocation($tipusPersonalitat);

            $dades = $this->filterMethodPersonalitat($dades, 'tipusPersonalitat', $tipusPersonalitat);
        }

        //validacion de datos del formulario
        if (!empty($denominacioSocial)) {
            $this->displayMethodInvocation($denominacioSocial);

            $dades = $this->filterMethodDenominacio($dades, 'denominacioSocial', $denominacioSocial);
        }

        $header = [
            // We make it sortable by name.
            ['data' => $this->t('Nom / Denominació Social')],
            ['data' => $this->t('Personalitat')],
            ['data' => $this->t('NIF/CIF')],
            ['data' => $this->t('Serveis')],
            ['data' => $this->t('+Info')],
        ];

        $rows = [];
        foreach ($dades as $dade) {

            $rows[] = array(
                'denominacioSocial' => $dade['denominacioSocial'],
                'tipusPersonalitat' => $dade['tipusPersonalitat'],
                'nif'               => $dade['nif'],
                'Serveis'           => $dade['Serveis'],
                'info'              => Link::createFromRoute(
                    $this->t('+ info'),
                    'cac_prestadors_llistat.info',
                    ['id' => $dade['idPrestador']],
                    [
                        'attributes' => [
                            'class'               => ['use-ajax'],
                            'data-dialog-type'    => 'modal',
                            'data-dialog-options' => json_encode(['height' => 500, 'width' => 800]),
                        ],
                    ]
                ),
                /*'info'              => Link::createFromRoute($this->t('dades'), 'cac_prestadors_llistat.info.tab1', ['id' => $dade['idPrestador']]),*/

            );

        }

        $this->displayMethodInvocation("count_rows " . count($rows));

        // Initialize the pager
        //$num_per_page = 20;
        //$current_page = pager_default_initialize(count($rows), $num_per_page);
        //$this->displayMethodInvocation("current_page " . $current_page);

        // Split your list into page sized chunks
        //$chunks = array_chunk($rows, $num_per_page, true);

        $form['table_wrapper'] = [
            '#type'       => 'container',
            '#attributes' => ['id' => 'table_wrapper'],
        ];

        // Build a render array which will be themed as a table with a pager.
        $form['table_wrapper']['table'] = array(
            '#rows'       => $rows,
            '#header'     => $header,
            '#type'       => 'table',
            '#empty'      => t('No content available.'),
            '#attributes' => ['id' => 'table_info', 'class' => ['dataTable display cell-border compact']],
        );
        /*$form['table_wrapper']['pager'] = array(
        '#type'       => 'pager',
        '#weight'     => 10,
        '#quantity'   => count($rows),
        '#route_name' => 'cac_prestadors_llistat.content',
        );*/

        /*************/

        \Drupal::logger('cac_prestadors')->notice('cacPrestadorsForm->buildForm() - fin -');
        // return parent::buildForm($form, $form_state);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        /*
    if ($form_state->getValue('cantidad') < 1) {
    $form_state->setErrorByName('cantidad', $this->t('La cantidad tiene que ser mayor que 1'));
    }
     */
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Display result.
        /*
    foreach ($form_state->getValues() as $key => $value) {
    drupal_set_message($key . ': ' . $value);
    }
     */
    }

    /**
     * Implements ajax submit callback.
     *
     * @param array $form
     *   Form render array.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Current state of the form.
     */
    public function ajaxSubmit(array &$form, FormStateInterface $form_state)
    {
        $this->displayMethodInvocation('ajaxSubmit');

        return $form['table_wrapper'];
    }

    public function filterMethodDenominacio($array, $index, $value)
    {
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {
                $temp[$key] = $array[$key][$index];

                if ($temp[$key] == $value) {
                    $newarray[$key] = $array[$key];
                }
            }
        }
        return $newarray;
    }

    public function filterMethodPersonalitat($array, $index, $value)
    {
        if (is_array($array) && count($array) > 0) {

            switch ($value) {
                case 1:
                    foreach (array_keys($array) as $key) {
                        $temp[$key] = $array[$key][$index];

                        if ($temp[$key] == 'Administració Pública') {
                            $newarray[$key] = $array[$key];
                        }
                    }
                    break;
                case 2:
                    foreach (array_keys($array) as $key) {
                        $temp[$key] = $array[$key][$index];

                        if ($temp[$key] != 'Administració Pública') {
                            $newarray[$key] = $array[$key];
                        }
                    }
                    break;
            }

            return $newarray;

        }

    }

}
