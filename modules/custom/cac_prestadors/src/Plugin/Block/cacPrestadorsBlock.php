<?php

namespace Drupal\cac_prestadors\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;

/**
 * Defines a custom block type.
 *
 * @Block(
 *  id = "cacPrestadorsLlistat",
 *  admin_label = @Translation("CAC Prestadors Llistat")
 * )
 */
class cacPrestadorsBlock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {

        $form = \Drupal::formBuilder()->getForm('Drupal\cac_prestadors\Form\cacPrestadorsForm');

        $build = [
            '#theme'      => 'pagina_prestadors_llistat',
            '#titol'      => $this->t("PRESTADORS QUE FORMEN PART DEL 'REGISTRE DE PRESTADORS' DEL CONSELL DE L'AUDIOVISUAL DE CATALUNYA"),
            '#descripcio' => 'Llistat de prestadors del CAC',
            '#formulari'  => $form,
            '#attached'   => [
                'library' => [
                    'cac_prestadors/cac',
                    'cac_prestadors/data-tables',
                ],
            ],
        ];
        

        return $build;

       
    }

}
