<?php

/**
 * @file
 * Contains \Drupal\cac_prestadors\Controller\cacPrestadorsController.
 */

namespace Drupal\cac_prestadors\Controller;

use Drupal;
/*use Drupal\Component\Render\FormattableMarkup;*/
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;

class cacPrestadorsController extends ControllerBase
{

    /**
     * Display the markup.
     *
     * @return array
     */

    public function prestadorsLlistat()
    {

        \Drupal::logger('cac_prestadors')->notice('*** prestadorsLlistat1: ');
        $form = \Drupal::formBuilder()->getForm('Drupal\cac_prestadors\Form\cacPrestadorsForm');

        \Drupal::logger('cac_prestadors')->notice('*** prestadorsLlistat2: ');

        $build = [
            '#theme'      => 'pagina_prestadors_llistat',
            '#titol'      => $this->t("PRESTADORS QUE FORMEN PART DEL 'REGISTRE DE PRESTADORS' DEL CONSELL DE L'AUDIOVISUAL DE CATALUNYA"),
            '#descripcio' => 'Llistat de prestadors del CAC',
            '#formulari'  => $form,
            '#attached'   => [
                'library' => [
                    'cac_prestadors/cac',
                    'cac_prestadors/data-tables',
                ],
            ],
        ];
        \Drupal::logger('cac_prestadors')->notice('*** prestadorsLlistat3: ');

        //\Drupal::logger('cac_prestadors')->notice('Passa 4: Retorn de theme = ');

        return $build;

    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorInfo($id = 0)
    {

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/infoPrestador/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_inf = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/participacions/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_par = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/decisions/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_dec = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/organsgovern/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_org = Json::decode($response);

        $uri         = "http://wsregpres.cac.cat/ws_regpres/api/llistaserveisprestador/id/" . $id;
        $response    = file_get_contents($uri);
        $dades_llist = Json::decode($response);

        $build = ['#theme'  => 'pagina_prestador_info',
            '#titol_inf'        => t('DADES DEL PRESTADOR'),
            '#descripcio_inf'   => t('Dades públiques del prestador'),
            '#dades_inf'        => $dades_inf,
            '#titol_par'        => t('DADES DE PARTICIPACIÓ DEL PRESTADOR'),
            '#descripcio_par'   => t('Dades públiques del prestador'),
            '#dades_par'        => $dades_par,
            '#titol_dec'        => t('DADES DECISIONS DEL PRESTADOR'),
            '#descripcio_dec'   => t('Dades públiques del prestador'),
            '#dades_dec'        => $dades_dec,
            '#titol_org'        => t('DADES GOVERN DEL PRESTADOR'),
            '#descripcio_org'   => t('Dades públiques del prestador'),
            '#dades_org'        => $dades_org,
            '#titol_llist'      => t('LLISTA SERVEIS DEL PRESTADOR'),
            '#descripcio_llist' => t('Dades públiques del prestador'),
            '#dades_llist'      => $dades_llist,
        ];

        return $build;
    }
}
