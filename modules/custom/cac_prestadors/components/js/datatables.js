(function($) {
    Drupal.behaviors.myModuleBehavior = {
        attach: function(context, settings) {
            $('#table_info').dataTable({
                pagingType: "full_numbers",
                order: [
                    [0, "asc"]
                ],
                language: {
                    url: '//cdn.datatables.net/plug-ins/1.10.15/i18n/Catalan.json'
                },
                retrieve: true,
                searching: false,
                dom: '<"top-table"i>rt<"bottom-table"flp><"clear">'
            });
        }
    };
})(jQuery);